import mongoengine


class PhraseUsed(mongoengine.EmbeddedDocument):
    id = mongoengine.ObjectIdField(primary_key=True)
    phrase_id = mongoengine.IntField()
    name = mongoengine.StringField()
    hits = mongoengine.IntField()
    errors = mongoengine.IntField()


class WordUsed(mongoengine.EmbeddedDocument):
    word_id = mongoengine.IntField()
    name = mongoengine.StringField()
    hits = mongoengine.IntField()
    errors = mongoengine.IntField()


class LogGames(mongoengine.EmbeddedDocument):
    game_id = mongoengine.IntField()
    used_words = mongoengine.EmbeddedDocumentListField(WordUsed)
    used_phrases = mongoengine.EmbeddedDocumentListField(PhraseUsed)


class LogSession(mongoengine.Document):
    session_date = mongoengine.DateTimeField()
    log_games = mongoengine.EmbeddedDocumentListField(LogGames)
    used_words = mongoengine.EmbeddedDocumentListField(WordUsed)
    used_phrases = mongoengine.EmbeddedDocumentListField(PhraseUsed)

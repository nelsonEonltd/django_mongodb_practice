from django.shortcuts import render
from comments.nosql_models import LogSession
from comments.serializers import LogSessionSerializer
from rest_framework_mongoengine import viewsets as no_sql_viewsets

# Create your views here.


class LogSessionViewSet(no_sql_viewsets.ModelViewSet):
    queryset = LogSession.objects.all()
    serializer_class = LogSessionSerializer

from comments.nosql_models import LogSession, PhraseUsed, WordUsed
from rest_framework_mongoengine import serializers as nosql_serializers


class PhraseUSedSerializer(nosql_serializers.EmbeddedDocumentSerializer):

    class Meta:
        model = PhraseUsed


class WordUSedSerializer(nosql_serializers.EmbeddedDocumentSerializer):

    class Meta:
        model = WordUsed


class LogSessionSerializer(nosql_serializers.DocumentSerializer):
    used_words = WordUSedSerializer(many=True)
    used_phrases = PhraseUSedSerializer(many=True)

    class Meta:
        fields = ('id', 'used_words', 'used_phrases')
        model = LogSession
